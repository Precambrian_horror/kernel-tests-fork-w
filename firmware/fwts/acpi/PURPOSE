Run fwts tests in the ACPI category (as returned by fwts --acpitests).
These run automatically by default and require no user intervention.

This includes the following tests (based on x86_64, may vary on other arches):

acpiinfo        General ACPI information test.
acpitables      ACPI table headers sanity tests.
apicinstance    Test for single instance of APIC/MADT table.
asf             ASF! Alert Standard Format Table test.
aspt            ASPT Table test.
bert            BERT Boot Error Record Table test.
bgrt            BGRT Boot Graphics Resource Table test.
boot            BOOT Table test.
checksum        ACPI table checksum test.
cpep            CPEP Corrected Platform Error Polling Table test.
csrt            CSRT Core System Resource Table test.
cstates         Processor C state support test.
dbg2            DBG2 (Debug Port Table 2) test.
dbgp            DBGP (Debug Port) Table test.
dmar            DMA Remapping (VT-d) test.
drtm            DRTM D-RTM Resources Table test.
ecdt            ECDT Embedded Controller Boot Resources Table test.
einj            EINJ Error Injection Table test.
erst            ERST Error Record Serialization Table test.
facs            FACS Firmware ACPI Control Structure test.
fadt            FADT Fixed ACPI Description Table tests.
fpdt            FPDT Firmware Performance Data Table test.
gtdt            GTDT Generic Timer Description Table test.
hest            HEST Hardware Error Source Table test.
hpet            HPET IA-PC High Precision Event Timer Table tests.
iort            IORT IO Remapping Table test.
lpit            LPIT Low Power Idle Table test.
madt            MADT Multiple APIC Description Table (spec compliant).
mcfg            MCFG PCI Express* memory mapped config space test.
mchi            MCHI Management Controller Host Interface Table test.
method          ACPI DSDT Method Semantic tests.
mpst            MPST Memory Power State Table test.
msct            MSCT Maximum System Characteristics Table test.
msdm            MSDM Microsoft Data Management Table test.
nfit            NFIT NVDIMM Firmware Interface Table test.
pcc             Processor Clocking Control (PCC) test.
pcct            PCCT Platform Communications Channel test.
pmtt            PMTT Memory Topology Table test.
rsdp            RSDP Root System Description Pointer test.
rsdt            RSDT Root System Description Table test.
sbst            SBST Smart Battery Specification Table test.
slic            SLIC Software Licensing Description Table test.
slit            SLIT System Locality Distance Information test.
spcr            SPCR Serial Port Console Redirection Table test.
spmi            SPMI Service Processor Management Interface Description Table test.
srat            SRAT System Resource Affinity Table test.
stao            STAO Status Override Table test.
tcpa            TCPA Trusted Computing Platform Alliance Capabilities Table test.
tpm2            TPM2 Trusted Platform Module 2 test.
uefi            UEFI Data Table test.
waet            WAET Windows ACPI Emulated Devices Table test.
wdat            WDAT Microsoft Hardware Watchdog Action Table test.
wmi             Extract and analyse Windows Management Instrumentation (WMI).
wpbt            WPBT Windows Platform Binary Table test.
xenv            XENV Xen Environment Table tests.
xsdt            XSDT Extended System Description Table test.

https://wiki.ubuntu.com/Kernel/Reference/fwts
